package com.pajato.argus.profile.adapter

import com.pajato.argus.profile.core.Profile

public fun getId(item: Profile): Int = item.id
