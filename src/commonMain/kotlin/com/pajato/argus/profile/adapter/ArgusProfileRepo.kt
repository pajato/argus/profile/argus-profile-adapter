package com.pajato.argus.profile.adapter

import com.pajato.argus.profile.core.Profile
import com.pajato.argus.profile.core.ProfileCache
import com.pajato.argus.profile.core.ProfileRepo
import com.pajato.argus.profile.core.ProfileRepoError
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.argus.profile.core.SelectableProfile.Companion.serializer
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.URI

public object ArgusProfileRepo : ProfileRepo {
    override val cache: ProfileCache = mutableMapOf()
    internal var profileUri: URI? = null

    private const val PROFILE_URI_ERROR = "No URI has been injected! A valid file URI is required."
    private const val DEFAULT_SELECTED = false
    private const val DEFAULT_HIDDEN = false

    override suspend fun injectDependency(uri: URI) {
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, serializer(), ::getKeyFromItem)
        profileUri = uri
        if (cache.isEmpty()) register(SelectableProfile(DEFAULT_SELECTED, DEFAULT_HIDDEN, Profile(0, "Argus")))
    }

    private fun handler(cause: Throwable) { throw cause }

    private fun getKeyFromItem(item: SelectableProfile) = item.profile.id

    override fun register(json: String) {
        val item = jsonFormat.decodeFromString(serializer(), json)
        updateCacheAndPersist(item, json)
    }

    override fun register(item: SelectableProfile) {
        val json = jsonFormat.encodeToString(serializer(), item)
        updateCacheAndPersist(item, json)
    }

    override fun toggleHidden(item: SelectableProfile): Unit = register(item.copy(isHidden = !item.isHidden))

    override fun toggleSelected(item: SelectableProfile): Unit = register(item.copy(isSelected = !item.isSelected))

    private fun updateCacheAndPersist(item: SelectableProfile, json: String) {
        cache[item.profile.id] = item
        cache[item.profile.id] = item
        val uri = profileUri ?: throw ProfileRepoError(PROFILE_URI_ERROR)
        runBlocking { launch(IO) { persist(uri, json) } }
    }
}
