package com.pajato.argus.profile.adapter

import com.pajato.test.ReportingTestProfiler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class CacheUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest
    fun setUp() {
        copyResourceDirs(loader, "read-only-files", "files")
        injectProfiles(loader, URI_ERROR)
    }

    @Test fun `When fetching repo data, verify a few cache operations`() {
        val item = ArgusProfileRepo.cache[0] ?: fail("Profile with id == 0 was not injected!")
        assertEquals(9, ArgusProfileRepo.cache.size)
        assertTrue(ArgusProfileRepo.cache[-1] == null)
        assertEquals(false, item.isSelected)
        assertEquals(0, item.profile.id)
        assertEquals("Paul", item.profile.label)
        assertEquals("", item.profile.iconUrl)
    }

    companion object {
        const val URI_ERROR = "Could not get the normal networks resource URI!"
    }
}
