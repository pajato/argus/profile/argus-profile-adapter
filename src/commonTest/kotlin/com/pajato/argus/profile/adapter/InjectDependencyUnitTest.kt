package com.pajato.argus.profile.adapter

import com.pajato.argus.profile.adapter.ArgusProfileRepo.injectDependency
import com.pajato.argus.profile.core.Profile
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.dependency.uri.validator.UriValidationError
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.io.path.toPath
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class InjectDependencyUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        copyResourceDirs(loader, "read-only-files", "files")
        ArgusProfileRepo.cache.clear()
        injectProfiles(loader, URI_ERROR)
    }

    @Test fun `When fetching profiles from a non-file, verify exception`() {
        val uri = loader.getResource("ProfilesDir-Empty")?.toURI() ?: fail("Invalid test URI!")
        val expected = "com.pajato.dependency.uri.validator.UriValidationError: java.lang.IllegalArgumentException: " +
            "The argument 'uri' is not a file URI!"
        runBlocking { assertFailsWith<UriValidationError> { injectDependency(uri) } }.also {
            assertEquals(expected, it.message)
        }
    }

    @Test fun `When fetching data from a persistence file that does not exist, verify the repo size is 1`() {
        val uri = loader.getResource("ProfilesDir-Empty")?.toURI() ?: fail("Invalid test directory URI!")
        val file = File(uri.toPath().toFile(), "no-such-file")
        runBlocking { injectDependency(file.toURI()) }
        assertEquals(1, ArgusProfileRepo.cache.size)
        file.delete()
    }

    @Test fun `When creating a new persistence file, verify the new repo size is 1`() {
        val uri = loader.getResource("ProfilesDir-NoFiles/dummy.txt")?.toURI() ?: fail("Invalid test URI!")
        runBlocking { injectDependency(uri) }
        assertEquals(1, ArgusProfileRepo.cache.size)
    }

    @Test fun `When injecting an empty file and registering a new profile, verify the repo size is 2`() {
        val resourceName = "ProfilesDir-Empty/profiles.txt"
        val uri = loader.getResource(resourceName)?.toURI() ?: fail("Invalid test directory URI!")
        val (isSelected, isHidden) = Pair(false, false)
        runBlocking { injectDependency(uri) }
        ArgusProfileRepo.register(SelectableProfile(isSelected, isHidden, Profile(27)))
        assertEquals(2, ArgusProfileRepo.cache.size)
    }

    private companion object { const val URI_ERROR = "Could not get the normal profiles resource URI!" }
}
