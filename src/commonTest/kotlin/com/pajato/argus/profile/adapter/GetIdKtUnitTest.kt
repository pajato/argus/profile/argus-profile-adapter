package com.pajato.argus.profile.adapter

import com.pajato.argus.profile.core.Profile
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class GetIdKtUnitTest : ReportingTestProfiler() {

    @Test fun `When an id is obtained for a profile via getId, verify the result`() {
        val profile = Profile(23)
        assertEquals(23, getId(profile))
    }
}
