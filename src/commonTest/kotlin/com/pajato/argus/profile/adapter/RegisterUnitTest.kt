package com.pajato.argus.profile.adapter

import com.pajato.argus.profile.adapter.ArgusProfileRepo.profileUri
import com.pajato.argus.profile.adapter.ArgusProfileRepo.register
import com.pajato.argus.profile.core.I18nStrings.PROFILE_URI_ERROR
import com.pajato.argus.profile.core.Profile
import com.pajato.argus.profile.core.ProfileRepoError
import com.pajato.argus.profile.core.SelectableProfile
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

class RegisterUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        copyResourceDirs(loader, "read-only-files", "files")
        injectProfiles(loader, URI_ERROR)
    }

    @Test fun `When registering a profile without injecting a URI using JSON data, verify the exception`() {
        val itemUrl = loader.getResource("item.json") ?: fail("Could not load the resource 'item.json'")
        val expected = get(PROFILE_URI_ERROR)
        profileUri = null
        assertFailsWith<ProfileRepoError> { register(itemUrl.readText()) }.also { assertEquals(expected, it.message) }
    }

    @Test fun `When registering a network without injecting a URI using item data, verify the exception`() {
        val (isSelected, isHidden) = Pair(false, false)
        val item = SelectableProfile(isSelected, isHidden, Profile(0))
        val expected = get(PROFILE_URI_ERROR)
        profileUri = null
        assertFailsWith<ProfileRepoError> { register(item) }.also { assertEquals(expected, it.message) }
    }

    @Test fun `When registering after injecting a valid URI, verify correct behavior`() {
        val itemUrl = loader.getResource("item.json") ?: fail("Could not load the resource 'item.json'")
        val json = itemUrl.readText().trim()
        register(json)
        assertCache()
    }

    private fun assertCache() {
        val item = ArgusProfileRepo.cache[0] ?: fail("Item with id == 0 failed to register!")
        assertEquals(9, ArgusProfileRepo.cache.size)
        assertEquals(true, item.isSelected)
        assertEquals("Pajato LLC", item.profile.label)
        assertEquals("https://pajato.com/8VCV78prwd9QzZnEm0ReO6bERDa.jpg", item.profile.iconUrl)
    }

    private companion object {
        const val URI_ERROR = "Could not get the normal networks resource URI!"
    }
}
