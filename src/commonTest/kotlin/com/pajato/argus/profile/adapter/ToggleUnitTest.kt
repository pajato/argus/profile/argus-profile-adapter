package com.pajato.argus.profile.adapter

import com.pajato.test.ReportingTestProfiler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class ToggleUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        copyResourceDirs(loader, "read-only-files", "files")
        injectProfiles(loader, URI_ERROR)
    }

    @Test fun `When toggling the isSelected property, verify behavior`() {
        fun getItem() = ArgusProfileRepo.cache[0] ?: fail("The item with id == 2552 was not injected!")
        val expected = getItem().isSelected.not()
        ArgusProfileRepo.toggleSelected(getItem())
        assertEquals(expected, getItem().isSelected)
    }

    @Test fun `When toggling the isHidden property, verify behavior`() {
        fun getItem() = ArgusProfileRepo.cache[0] ?: fail("The item with id == 2552 was not injected!")
        val expected = getItem().isHidden.not()
        ArgusProfileRepo.toggleHidden(getItem())
        assertEquals(expected, getItem().isHidden)
    }

    private companion object { const val URI_ERROR = "Could not get the normal networks base resource URI!" }
}
