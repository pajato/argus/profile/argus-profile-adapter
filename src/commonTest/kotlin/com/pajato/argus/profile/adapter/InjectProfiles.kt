package com.pajato.argus.profile.adapter

import com.pajato.argus.profile.core.I18nStrings.registerStrings
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.fail

internal fun injectProfiles(loader: ClassLoader, errorMessage: String) {
    val name = "files/profiles.txt"
    val uri: URI = loader.getResource(name)?.toURI() ?: fail(errorMessage)
    registerStrings()
    runBlocking { ArgusProfileRepo.injectDependency(uri) }
}
