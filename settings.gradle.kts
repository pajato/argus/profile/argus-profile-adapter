@file:Suppress("UnstableApiUsage")

rootProject.name = "argus-profile-adapter"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        mavenLocal()
    }

    plugins { id("com.pajato.plugins.kmp-lib") version "0.9.2" apply false }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
}
