plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato.argus"
version = "0.10.4"
description = "The Argus profile feature, interfaces adapter (adapter) layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting {
        dependencies {
            implementation(libs.kotlinx.coroutines.core)
            implementation(libs.kotlinx.serialization.json)

            implementation(libs.pajato.persister)
            implementation(libs.pajato.i18n.strings)
            implementation(libs.pajato.uri.validator)

            implementation(libs.argus.profile.core)
            implementation(libs.argus.shared.core)
        }
    }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
